import random


suits = ('Inima Rosie', 'Romb', 'Inima Neagra', 'Trefla')
ranks = ('Sapte', 'Opt', 'Noua', 'Zece', 'Juva', 'Dama', 'Popa', 'As')
values = {'Sapte':7, 'Opt':8, 'Noua':9, 'Zece':10, 'Juva':11, 'Dama':12, 'Popa':13, 'As':14}

playing = True

class Carte:
    
    def __init__(self,suit,rank):
        self.suit = suit
        self.rank = rank
        
    def __str__(self):
        return self.rank + ' de ' + self.suit

class Pachet:
    
    def __init__(self):
        self.pachet = []  # start with an empty list
        for suit in suits:
            for rank in ranks:
                self.pachet.append(Carte(suit,rank))
                
    def __str__(self):
        pachet_comp = ''  # start with an empty string
        for carte in self.pachet:
            pachet_comp += '\n '+carte.__str__() # add each Card object's print string
        return 'The deck has:' + pachet_comp
                
    def shuffle(self):
        random.shuffle(self.pachet)
        
    def deal(self):
        single_card = self.pachet.pop()
        return single_card

    
class Inainte_de_joc():

    def __init__(self):
        self.pachet = pachet

    def Taie(self):
        count_7 = 0
        while True:
            try:
                x = int(input('Cine taie papa coaie: '))
            except ValueError | x < 0 | x > 35:
                print('Ai taiat prost andicapatule')
            else:
                break

    